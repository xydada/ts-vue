import { createApp } from 'vue'
import 'normalize.css'
import './assets/css/index.less'
import App from './App.vue'
import router from './router'
import pinia from './store'
import registerIcons from '@/global/register-icons'
import useLoginStore from '@/store/login/login'

// 3.图标的全局注册

const app = createApp(App)
app.use(pinia)
app.use(registerIcons)
const loginStore = useLoginStore()
loginStore.loadLocalCacheAction()
app.use(router)
app.mount('#app')
