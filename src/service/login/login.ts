import type { IAccount } from '@/types'
import hyRequest from '@/service'

export function demo() {
  return hyRequest.get({
    url: '/home/list'
  })
}

export function accountLoginRequest(account: IAccount) {
  return hyRequest.post({
    url: '/login',
    data: account
  })
}

/**
 * 获取当前登录用户详情数据
 */
export function getUserInfoById() {
  return hyRequest.get({
    url: `/getInfo`
  })
}

/**
 * 根据当前登录用户获取其对应的菜单权限数据
 */
export function getUserMenusByLoginUser() {
  return hyRequest.get({
    url: '/getRouters'
  })
}
